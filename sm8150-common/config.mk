PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/sm8150-common

PRODUCT_COPY_FILES += \
    vendor/oneplus/apps/sm8150-common/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/oneplus/apps/sm8150-common/proprietary/system_ext/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oneplus.camera.xml \
    vendor/oneplus/apps/sm8150-common/proprietary/system_ext/etc/configs/hiddenapi-package-whitelist-oneplus.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist-oneplus.xml \
    vendor/oneplus/apps/sm8150-common/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so

PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService
